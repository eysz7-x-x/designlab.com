module.exports = {
  extends: ['@gitlab'],
  rules: {
    'filenames/match-regex': 'off',
    'no-param-reassign': 'off',
    'import/no-extraneous-dependencies': 'off'
  }
};
