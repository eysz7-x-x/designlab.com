---
name: Radio buttons
related:
  - checkboxes
  - segmented-control
  - toggles
---

Radio buttons are an input which acts as a select list. Every radio button acts as a boolean, but only one radio button can be selected at any time - one is active by default.

## Usage

Radio buttons can never be used alone and must always be used in group form. A vertical layout is preferred, with one choice per line. If you must use a horizontal layout, with multiple options per line, make sure to to space the buttons and labels enough so there is a clear separation between selections.

Use radio buttons when there is a choice between two or multiple selections, it has a default state, and the user needs a confirmation of it being saved.

Radio buttons may replace segmented controls, a list of checkboxes, or a toggle to allow users to choose between two or multiple options. For help with choosing the right solution, follow the table below.

Todo: Add replacement-comparison-table

### Labels

Radio button labels are set in regular font weight, positioned to the right of the element, and should be as short as possible. Group labels are set in bold font weight and positioned above the group of radio buttons. [Help text](/components/forms#help-text) can be added below the radio button labels or as a paragraph below the group label.

### Visual Design

Radio buttons should use high-contrast colors and a dot icon to indicate the states - selected and unselected.

### Interaction

Users are able to select an option by clicking on either the radio button itself or on its label.

## Demo

Todo: Add live component block with code example

## Design specifications

Color, spacing, dimension, and layout specific information pertaining to this component can be viewed using the following link:

[Sketch Measure Preview for radio buttons](https://gitlab-org.gitlab.io/gitlab-design/hosted/design-gitlab-specs/radiobuttons-spec-previews/)
