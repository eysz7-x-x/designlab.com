---
name: Alerts
related:
  - broadcast-messages
  - toasts
---

Alerts allow the application to pass along relevant information to the user without impeding their journey. Alerts are not error validations and should not be used as a substitute for errors.

## Usage

|Component type|Purpose|
|--- |--- |
|Danger alerts|To be used for critical warnings that require the user's attention.|
|Warning alerts|To be used for other non-critical warnings that require the users attention.|
|Information alerts|To be used to display important information to the user related to their task.|
|Tip alerts|To be used to display tips on using GitLab, new/unused features, and other marketing information.|
|Success alerts|To be used to alert the user that the action they have completed was successful.|

## Demo

The alert pattern is designed to be flexible and accounts for as many use cases as possible.

An example of a live alert related to a usage guideline.

## Design specifications

Color, spacing, dimension, and layout specific information pertaining to this component can be viewed using the following link:

[Sketch Measure Preview for Alerts](https://gitlab-org.gitlab.io/gitlab-design/hosted/design-gitlab-specs/alerts-spec-previews/)
